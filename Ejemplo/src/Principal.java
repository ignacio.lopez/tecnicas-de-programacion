import javax.swing.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Principal {

    private static List<Vehiculo> vehiculos= new ArrayList();

    public static void main(String[] args) {
        String menu = "Bienvenido. Ingrese una opción:\n"+
                "1. Registrar vehículo.\n"+
                "2. Registrar revisión.\n"+
                "3. Consultar vehículo.\n"+
                "4. Salir.";

        String opcion="";

        do{
            opcion= JOptionPane.showInputDialog(menu);
            switch (opcion){
                case "1": //registrar un vehículo
                    String placa = JOptionPane.showInputDialog("Ingrese la placa:");
                    String marca = JOptionPane.showInputDialog("Ingrese la marca:");
                    String modelo = JOptionPane.showInputDialog("Ingrese el modelo:");
                    int anio = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el año:"));
                    String color = JOptionPane.showInputDialog("Ingrese el color:");
                    int numeroPuertas = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el número de puertas:"));
                    Vehiculo v = new Vehiculo(placa, marca, modelo, anio, color, numeroPuertas);
                    vehiculos.add(v);
                    JOptionPane.showMessageDialog(null, "Vehículo registrado correctamente");
                    break;
                case "2": //registrar una revisión
                    String placaRevision = JOptionPane.showInputDialog("Ingrese la placa del vehículo revisado:");
                    Optional<Vehiculo> vehiculoRevision= Optional.empty();
                    for(Vehiculo vehiculo:vehiculos){
                        if(placaRevision.equals(vehiculo.getPlaca())){
                           vehiculoRevision = Optional.of(vehiculo);
                           break;
                        }
                    }
                    if(!vehiculoRevision.isPresent()){
                        JOptionPane.showMessageDialog(null, "El vehículo con la placa ingresada no existe");
                    }else{
                        String diagnostico = JOptionPane.showInputDialog("Ingrese el diagnóstico: ");
                        Revision revision = new Revision(LocalDateTime.now(), diagnostico);
                        Vehiculo vehiculo = vehiculoRevision.get();
                        vehiculo.getRevisiones().add(revision);
                        JOptionPane.showMessageDialog(null, "Revisión registrada correctamente");
                    }
                    break;
                case "3": //consultar vehículo
                    String placaConsulta = JOptionPane.showInputDialog("Ingrese la placa del vehículo a consultar:");
                    for(Vehiculo vehiculo:vehiculos){
                        if(placaConsulta.equals(vehiculo.getPlaca())){
                            System.out.println(vehiculo);
                            break;
                        }
                    }
                    break;

                case "4":
                    JOptionPane.showMessageDialog(null, "Hasta luego");
                    break;
                default: //error
                    JOptionPane.showMessageDialog(null, "Opción incorrecta");
            }

        }while (!"4".equals(opcion));
    }
}
