package co.edu.udea.tecnicas.mascotas.dao.impl;

import co.edu.udea.tecnicas.mascotas.dao.MascotaDAO;
import co.edu.udea.tecnicas.mascotas.model.Mascota;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static java.nio.file.StandardOpenOption.APPEND;

public class MascotaDAOFile implements MascotaDAO {

    private final static int LONGITUD_REGISTRO = 140;
    private final static int LONGITUD_RAZA = 50;
    private final static int LONGITUD_NOMBRE=50;
    private final static int LONGITUD_TIPO = 20;
    private final static int LONGITUD_ID=20;

    private final static String NOMBRE_ARCHIVO = "mascotas";

    private final static Path archivo = Paths.get(NOMBRE_ARCHIVO);

    public MascotaDAOFile(){
        if (!Files.exists(archivo)) {
            try {
                Files.createFile(archivo);
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }



    @Override
    public void guardarMascota(Mascota mascota) {
        String registro = parseMascota2String(mascota);
        byte[] datosRegistro = registro.getBytes();
        ByteBuffer byteBuffer = ByteBuffer.wrap(datosRegistro);
        try (FileChannel fc = (FileChannel.open(archivo, APPEND))) {
            fc.write(byteBuffer);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    @Override
    public List<Mascota> listarMascotas() {
        return null;
    }


    private String parseMascota2String(Mascota mascota) {
        StringBuilder registro = new StringBuilder();
        registro.append(completarCampo(mascota.getRaza(), LONGITUD_RAZA));
        registro.append(completarCampo(mascota.getNombre(), LONGITUD_NOMBRE));
        registro.append(completarCampo(mascota.getTipo(), LONGITUD_TIPO));
        registro.append(completarCampo(mascota.getId(), LONGITUD_ID));
        return registro.toString();
    }

    private String completarCampo(String campo, int longitud) {
        if (campo.length() > longitud) {
            return campo.substring(0, longitud);
        }
        return String.format("%1$-" + longitud + "s", campo);
    }
}
