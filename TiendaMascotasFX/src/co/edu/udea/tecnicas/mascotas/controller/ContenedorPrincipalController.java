package co.edu.udea.tecnicas.mascotas.controller;

import co.edu.udea.tecnicas.mascotas.controller.base.BaseController;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;

import java.io.IOException;

public class ContenedorPrincipalController {

    private static final String VIEWS_PATH = "../view/";

    @FXML
    private BorderPane contenedorPrincipal;

    @FXML
    private void mnuArchivoSalir_action() {
        System.exit(0);
    }

    @FXML
    private void mnuRegistrarMascota_action() {
        cambiarVentana(VentanasEnum.REGISTRAR_MASCOTA.nombre);
    }

    @FXML
    private void mnuListarMascotas_action() {
        cambiarVentana(VentanasEnum.LISTAR_MASCOTA.nombre);
    }

    public void cambiarVentana(String nombreVista) {
        cambiarVentana(nombreVista, null);
    }

    /**
     * Método encargado de cargar una vista en el centro del borderpane referenciado por el contenedor principal
     * @param nombreVista nombre de la vista a pintar en el centro
     * @param mensaje mensaje a entregarle a la vista al momento de cargarla (puede ser nulo)
     */
    public void cambiarVentana(String nombreVista, Object mensaje) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource(
                    VIEWS_PATH + nombreVista + ".fxml"));
            AnchorPane registrarMascota = loader.load();
            contenedorPrincipal.setCenter(registrarMascota);
            BaseController controller = loader.getController();
            controller.setContenedorPadre(this);
            controller.procesarMensaje(mensaje);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Enumeración para todas las vistas que gestionará el contenedor principal
     */
    public enum VentanasEnum {
        REGISTRAR_MASCOTA("registrar-mascota"), LISTAR_MASCOTA("listar-mascotas");

        private String nombre;

        VentanasEnum(String nombre) {
            this.nombre = nombre;
        }

        public String getNombre() {
            return this.nombre;
        }
    }


}
