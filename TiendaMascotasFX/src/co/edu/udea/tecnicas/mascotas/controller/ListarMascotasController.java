package co.edu.udea.tecnicas.mascotas.controller;

import co.edu.udea.tecnicas.mascotas.bsn.MascotaBsn;
import co.edu.udea.tecnicas.mascotas.controller.base.BaseController;
import co.edu.udea.tecnicas.mascotas.model.Mascota;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

import java.util.List;
import java.util.Objects;

public class ListarMascotasController extends BaseController {

    @FXML
    private TableView<Mascota> tblMascotas;
    @FXML
    private TableColumn<Mascota, String> clmId;
    @FXML
    private TableColumn<Mascota, String> clmTipo;
    @FXML
    private TableColumn<Mascota, String> clmRaza;
    @FXML
    private TableColumn<Mascota, String> clmNombre;

    //negocio
    private MascotaBsn mascotaBsn;

    private Mascota mascotaSeleccionada;

    public ListarMascotasController(){
        //cargar mascotas
        this.mascotaBsn=new MascotaBsn();
    }

    @FXML
    private void initialize(){
        //consultar listado de mascotas
        List<Mascota> mascotasList = mascotaBsn.listarMascotas();
        ObservableList<Mascota> mascotasObservables = FXCollections.observableList(mascotasList);
        tblMascotas.setItems(mascotasObservables);
        clmId.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getId()));
        clmTipo.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getTipo()));
        clmRaza.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getRaza()));
        clmNombre.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getNombre()));

        tblMascotas.getSelectionModel().selectedItemProperty()
                .addListener(((observable, oldValue, newValue) -> seleccionarMascota(newValue)));

    }

    private void seleccionarMascota(Mascota mascotaSeleccionada) {
        this.mascotaSeleccionada=mascotaSeleccionada;
    }

    @FXML
    public void btnActualizar_clic(){
        if(Objects.nonNull(this.mascotaSeleccionada)){
            contenedorPadre
                    .cambiarVentana(ContenedorPrincipalController.VentanasEnum.REGISTRAR_MASCOTA.getNombre(), mascotaSeleccionada);
            return;
        }

        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Actualización de mascotas");
        alert.setHeaderText("Por favor seleccione la mascota a actualizar ¬¬'");
        alert.setContentText("");
        alert.showAndWait();
    }


    @Override
    public void procesarMensaje(Object mensaje) {
        System.out.println("no se hace nada");
    }
}
