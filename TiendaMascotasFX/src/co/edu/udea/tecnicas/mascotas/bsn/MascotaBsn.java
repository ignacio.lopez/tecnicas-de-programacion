package co.edu.udea.tecnicas.mascotas.bsn;

import co.edu.udea.tecnicas.mascotas.bsn.exception.MascotaYaExisteException;
import co.edu.udea.tecnicas.mascotas.dao.MascotaDAO;
import co.edu.udea.tecnicas.mascotas.dao.exception.LlaveDuplicadaException;
import co.edu.udea.tecnicas.mascotas.dao.impl.MascotaDAOFile;
import co.edu.udea.tecnicas.mascotas.dao.impl.MascotaDAOList;
import co.edu.udea.tecnicas.mascotas.model.Mascota;

import java.util.List;

public class MascotaBsn {
    private MascotaDAO mascotaDAO;

    public MascotaBsn(){
        mascotaDAO = new MascotaDAOList();
    }

    public void guardarMascota(Mascota mascota) {
        //todo validar reglas de negocio


            mascotaDAO.guardarMascota(mascota);

    }

    public List<Mascota> listarMascotas() {
        return mascotaDAO.listarMascotas();
    }
}
