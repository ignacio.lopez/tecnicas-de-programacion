public abstract class Animal {

    public static int CONTADOR_ANIMALES = 0;

    public Animal(){
        CONTADOR_ANIMALES+=1;
        System.out.println("Animales creados "+CONTADOR_ANIMALES);
    }

    public abstract String hacerRuido();
}
