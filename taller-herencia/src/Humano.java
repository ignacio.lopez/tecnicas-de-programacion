public class Humano extends Animal {

    public static int CONTADOR_HUMANOS = 0;

    public Humano() {
        CONTADOR_HUMANOS++;
        System.out.println("Humanos creados " + CONTADOR_HUMANOS);
    }

    @Override
    public String hacerRuido() {
        return "blah! blah! blah!";
    }

    public String trabajar() {
        return "trabajo como humano";
    }
}
