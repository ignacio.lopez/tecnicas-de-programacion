package co.edu.udea.tecnicas.bsn;

import co.edu.udea.tecnicas.bsn.exception.NegocioException;
import co.edu.udea.tecnicas.bsn.exception.ObjetoYaExisteException;
import co.edu.udea.tecnicas.dao.EstudianteDAO;
import co.edu.udea.tecnicas.dao.NotaDAO;
import co.edu.udea.tecnicas.dao.exception.LlaveDuplicadaException;
import co.edu.udea.tecnicas.dao.impl.EstudianteDAONio;
import co.edu.udea.tecnicas.dao.impl.NotaDAONio;
import co.edu.udea.tecnicas.model.Estudiante;
import co.edu.udea.tecnicas.model.Nota;

import java.util.List;
import java.util.OptionalDouble;

public class EstudianteBsn {

    private EstudianteDAO estudianteDAO;
    private NotaDAO notaDAO;

    public EstudianteBsn() {
        this.estudianteDAO = new EstudianteDAONio();
        this.notaDAO = new NotaDAONio();
    }


    public void registrarEstudiante(Estudiante estudiante) throws ObjetoYaExisteException {
        // todo validar reglas de negocio
        try {
            this.estudianteDAO.registrarEstudiante(estudiante);
        } catch (LlaveDuplicadaException lde) {
            System.out.println(lde);
            throw new ObjetoYaExisteException(String
                    .format("El estudiante con id: %s ya había sido creado", estudiante.getId()));
        }
    }

    public List<Estudiante> listarEstudiantes() {
        return this.estudianteDAO.listarEstudiantes();
    }

    public void registrarNota(Nota nota) throws ObjetoYaExisteException, NegocioException {
        // se validan las reglas de negocio
        List<Nota> notasExistentes = this.notaDAO.consultarNotas(nota.getIdEstudiante());
        Double porcentajeAcumulado = notasExistentes.stream()
                .map(n -> n.getPorcentaje())
                .reduce(0.0, Double::sum);

        double nuevoPorcentajeAcumulado = porcentajeAcumulado + nota.getPorcentaje();

        if (nuevoPorcentajeAcumulado > 100) {
            throw new NegocioException(String.format("El porcentaje que intenta ingresar superaría el 100%% (%.1f%%)", nuevoPorcentajeAcumulado));
        }

        if (nota.getValor() < 0.0 || nota.getValor() > 5.0) {
            throw new NegocioException("La nota debe estar entre 0 y 5");
        }
        try {
            this.notaDAO.registrarNota(nota);
        } catch (LlaveDuplicadaException lde) {
            System.out.println(lde);
            throw new ObjetoYaExisteException(String
                    .format("Una nota con la descripción:: %s ya había sido creada", nota.getDescripcion()));

        }
    }

    public List<Nota> consultarNotas(String id) {
        return this.notaDAO.consultarNotas(id);

    }

    public double obtenerPromedioAcumulado(String id) {
        List<Nota> notasExistentes = this.notaDAO.consultarNotas(id);
        double promedio = notasExistentes.stream()
                .mapToDouble(n -> n.getValor() * n.getPorcentaje() / 100)
                .sum();
        return promedio;
    }

    public double obtenerPromedioGeneral(String id) {
        List<Nota> notasExistentes = this.notaDAO.consultarNotas(id);
        OptionalDouble promedio = notasExistentes.stream()
                .mapToDouble(n -> n.getValor())
                .average();
        if (promedio.isPresent()) {
            return promedio.getAsDouble();
        }
        return 0.0;
    }
}
