package co.edu.udea.tecnicas.bsn.exception;

public class ObjetoYaExisteException extends Exception {

    public ObjetoYaExisteException(String mensaje){
        super(mensaje);
    }
}
