package co.edu.udea.tecnicas.dao;

import co.edu.udea.tecnicas.dao.exception.LlaveDuplicadaException;
import co.edu.udea.tecnicas.model.Estudiante;

import java.util.List;
import java.util.Optional;

public interface EstudianteDAO {

    void registrarEstudiante(Estudiante estudiante) throws LlaveDuplicadaException;

    Optional<Estudiante> consultarEstudiantePorId(String id);

    List<Estudiante> listarEstudiantes();
}
