package co.edu.udea.tecnicas.dao;

import co.edu.udea.tecnicas.dao.exception.LlaveDuplicadaException;
import co.edu.udea.tecnicas.model.Nota;

import java.util.List;

public interface NotaDAO {

    void registrarNota(Nota nota) throws LlaveDuplicadaException;

    List<Nota> consultarNotas(String idEstudiante);
}
