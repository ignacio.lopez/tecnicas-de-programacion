package co.edu.udea.tecnicas.dao.impl;

import co.edu.udea.tecnicas.dao.NotaDAO;
import co.edu.udea.tecnicas.dao.exception.LlaveDuplicadaException;
import co.edu.udea.tecnicas.model.Nota;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.nio.file.StandardOpenOption.APPEND;

public class NotaDAONio implements NotaDAO {

    private final static String NOMBRE_ARCHIVO = "notas";
    private final static Path ARCHIVO = Paths.get(NOMBRE_ARCHIVO);
    private final static String FIELD_SEPARATOR = ",";
    private final static String RECORD_SEPARATOR = System.lineSeparator();

    public NotaDAONio() {
        if (!Files.exists(ARCHIVO)) {
            try {
                Files.createFile(ARCHIVO);
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }


    @Override
    public void registrarNota(Nota nota) throws LlaveDuplicadaException {
        String notaString = parseNota2String(nota);
        byte[] datosRegistro = notaString.getBytes();
        ByteBuffer byteBuffer = ByteBuffer.wrap(datosRegistro);
        try (FileChannel fileChannel = FileChannel.open(ARCHIVO, APPEND)) {
            fileChannel.write(byteBuffer);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    private String parseNota2String(Nota nota) {
        StringBuilder sb = new StringBuilder();
        sb.append(nota.getDescripcion()).append(FIELD_SEPARATOR)
                .append(nota.getPorcentaje()).append(FIELD_SEPARATOR)
                .append(nota.getValor()).append(FIELD_SEPARATOR)
                .append(nota.getIdEstudiante()).append(RECORD_SEPARATOR);
        return sb.toString();
    }

    @Override
    public List<Nota> consultarNotas(String idEstudiante) {
        List<Nota> notas = new ArrayList<>();
        try (Stream<String> stream = Files.lines(ARCHIVO)) {
            notas = stream.filter(notaString -> idEstudiante.equals(notaString.split(",")[3]))
                    .map(this::parseNota2Object)
                    .collect(Collectors.toList());
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return notas;
    }

    private Nota parseNota2Object(String notaString) {
        String[] datosNota = notaString.split(FIELD_SEPARATOR);
        // todo: validar que el tamaño del arreglo sea de 4 elementos
        Nota nota = new Nota(datosNota[0],
                Double.parseDouble(datosNota[1]), Double.parseDouble(datosNota[2]));
        nota.setIdEstudiante(datosNota[3]);
        return nota;

    }

}
