package co.edu.udea.tecnicas.dao.impl;

import co.edu.udea.tecnicas.dao.EstudianteDAO;
import co.edu.udea.tecnicas.dao.exception.LlaveDuplicadaException;
import co.edu.udea.tecnicas.model.Estudiante;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class EstudianteDAOList implements EstudianteDAO {

    private static List<Estudiante> bd = new ArrayList();

    @Override
    public void registrarEstudiante(Estudiante estudiante) throws LlaveDuplicadaException {
        Optional<Estudiante> estudianteOptional = consultarEstudiantePorId(estudiante.getId());
        if(estudianteOptional.isPresent()){
            throw new LlaveDuplicadaException(estudiante.getId());
        }
        bd.add(estudiante);
    }

    @Override
    public Optional<Estudiante> consultarEstudiantePorId(String id) {
        return bd.stream()
                .filter(estudiante -> estudiante.getId().equals(id))
                .findFirst();
    }

    @Override
    public List<Estudiante> listarEstudiantes() {
        return new ArrayList<>(bd);
    }


}
