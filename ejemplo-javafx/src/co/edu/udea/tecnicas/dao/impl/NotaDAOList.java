package co.edu.udea.tecnicas.dao.impl;

import co.edu.udea.tecnicas.dao.NotaDAO;
import co.edu.udea.tecnicas.dao.exception.LlaveDuplicadaException;
import co.edu.udea.tecnicas.model.Nota;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class NotaDAOList implements NotaDAO {

    private static List<Nota> bd = new ArrayList<>();

    @Override
    public void registrarNota(Nota nota) throws LlaveDuplicadaException {
        bd.add(nota);
    }

    @Override
    public List<Nota> consultarNotas(String idEstudiante) {
        return bd.stream()
                .filter(nota-> nota.getIdEstudiante().equals(idEstudiante))
                .collect(Collectors.toList());
    }
}
