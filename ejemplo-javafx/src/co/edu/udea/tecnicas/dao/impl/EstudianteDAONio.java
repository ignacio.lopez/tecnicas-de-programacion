package co.edu.udea.tecnicas.dao.impl;

import co.edu.udea.tecnicas.dao.EstudianteDAO;
import co.edu.udea.tecnicas.dao.exception.LlaveDuplicadaException;
import co.edu.udea.tecnicas.model.Estudiante;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static java.nio.file.StandardOpenOption.APPEND;

public class EstudianteDAONio implements EstudianteDAO {

    private final static String NOMBRE_ARCHIVO = "estudiantes";
    private final static Path ARCHIVO = Paths.get(NOMBRE_ARCHIVO);
    private final static String FIELD_SEPARATOR = ",";
    private final static String RECORD_SEPARATOR = System.lineSeparator();

    public EstudianteDAONio() {
        if (!Files.exists(ARCHIVO)) {
            try {
                Files.createFile(ARCHIVO);
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }


    @Override
    public void registrarEstudiante(Estudiante estudiante) throws LlaveDuplicadaException {
        Optional<Estudiante> estudianteOptional = this.consultarEstudiantePorId(estudiante.getId());
        if(estudianteOptional.isPresent()){
            throw new LlaveDuplicadaException(estudiante.getId());
        }
        String estudianteString = parseEstudiante2String(estudiante);
        byte[] datosRegistro = estudianteString.getBytes();
        ByteBuffer byteBuffer = ByteBuffer.wrap(datosRegistro);
        try (FileChannel fileChannel = FileChannel.open(ARCHIVO, APPEND)) {
            fileChannel.write(byteBuffer);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    private String parseEstudiante2String(Estudiante estudiante) {
        StringBuilder sb = new StringBuilder();
        sb.append(estudiante.getId()).append(FIELD_SEPARATOR)
                .append(estudiante.getNombres()).append(FIELD_SEPARATOR)
                .append(estudiante.getApellidos()).append(FIELD_SEPARATOR)
                .append(estudiante.getProgramaAcademico()).append(RECORD_SEPARATOR);
        return sb.toString();
    }

    @Override
    public Optional<Estudiante> consultarEstudiantePorId(String id) {
        try (Stream<String> stream = Files.lines(ARCHIVO)) {
            Optional<String> estudianteString = stream
                    .filter(estudiante-> id.equals(estudiante.split(",")[0]))
                    .findFirst();
            if(estudianteString.isPresent()){
                return Optional.of(parseEstudiante2Object(estudianteString.get()));
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return Optional.empty();
    }

    @Override
    public List<Estudiante> listarEstudiantes() {
        List<Estudiante> estudiantes = new ArrayList<>();
        try (Stream<String> stream = Files.lines(ARCHIVO)) {
            stream.forEach(estudianteString -> estudiantes.add(parseEstudiante2Object(estudianteString)));
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return estudiantes;
    }

    private Estudiante parseEstudiante2Object(String estudianteString) {
        String[] datosEstudiante = estudianteString.split(FIELD_SEPARATOR);
        // todo: validar que el tamaño del arreglo sea de 4 elementos
        Estudiante estudiante = new Estudiante(datosEstudiante[0],
                datosEstudiante[1], datosEstudiante[2], datosEstudiante[3]);
        return estudiante;

    }

}
