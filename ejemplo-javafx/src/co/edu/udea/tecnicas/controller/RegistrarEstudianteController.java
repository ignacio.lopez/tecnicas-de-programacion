package co.edu.udea.tecnicas.controller;

import co.edu.udea.tecnicas.bsn.EstudianteBsn;
import co.edu.udea.tecnicas.bsn.exception.ObjetoYaExisteException;
import co.edu.udea.tecnicas.model.Estudiante;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;

public class RegistrarEstudianteController {

    @FXML
    private TextField txtIdentificacion;
    @FXML
    private TextField txtNombres;
    @FXML
    private TextField txtApellidos;
    @FXML
    private TextField txtProgramaAcademico;

    private EstudianteBsn estudianteBsn = new EstudianteBsn();

    @FXML
    public void initialize() {
        txtIdentificacion.setTextFormatter(new TextFormatter<>(change -> {
            if (change.getControlNewText().matches("([1-9][0-9]*)?") && change.getControlNewText().length() <= 12) {
                return change;
            }
            return null;
        }));
    }


    public void btnGuardar_action() {
        String idIngresado = txtIdentificacion.getText().trim();
        String nombresIngresados = txtNombres.getText().trim();
        String apellidosIngresados = txtApellidos.getText().trim();
        String programaAcademicoIngresado = txtProgramaAcademico.getText().trim();

        // todo revisar espacios
        boolean esValido = validarCampos(idIngresado, nombresIngresados, apellidosIngresados, programaAcademicoIngresado);

        if (!esValido) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Registro de estudiante");
            alert.setHeaderText("Registro de estudiante");
            alert.setContentText("Diligencie todos los campos");
            alert.showAndWait();
            return;
        }

        Estudiante estudiante = new Estudiante(idIngresado, nombresIngresados, apellidosIngresados, programaAcademicoIngresado);
        try {
            estudianteBsn.registrarEstudiante(estudiante);
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Registro de estudiante");
            alert.setHeaderText("Resultado de la operación");
            alert.setContentText("El registro ha sido exitoso");
            alert.showAndWait();
            limpiarCampos();
        } catch (ObjetoYaExisteException oyee) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Registro de estudiante");
            alert.setHeaderText("Ha ocurrido un error");
            alert.setContentText(oyee.getMessage());
            alert.showAndWait();
        }


    }

    private boolean validarCampos(String... campos) {
        for (int i = 0; i < campos.length; i++) {
            if (campos[i] == null || "".equals(campos[i])) {
                return false;
            }
        }
        return true;
    }

    private void limpiarCampos() {
        txtIdentificacion.clear();
        txtNombres.clear();
        txtApellidos.clear();
        txtProgramaAcademico.clear();
    }

}
