package co.edu.udea.tecnicas.controller;

import co.edu.udea.tecnicas.bsn.EstudianteBsn;
import co.edu.udea.tecnicas.bsn.exception.NegocioException;
import co.edu.udea.tecnicas.bsn.exception.ObjetoYaExisteException;
import co.edu.udea.tecnicas.model.Estudiante;
import co.edu.udea.tecnicas.model.Nota;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.util.List;

public class RegistrarNotaController {

    @FXML
    private ComboBox<Estudiante> cmbEstudiantes;
    @FXML
    private TextField txtDescripcion;
    @FXML
    private TextField txtPorcentaje;
    @FXML
    private TextField txtValor;
    @FXML
    private TableView<Nota> tblNotas;
    @FXML
    private TableColumn<Nota, String> clmDescripcion;
    @FXML
    private TableColumn<Nota, Double> clmPorcentaje;
    @FXML
    private TableColumn<Nota, Double> clmNota;
    @FXML
    private Label lblPromedioAcumulado;
    @FXML
    private Label lblPromedioGeneral;

    private EstudianteBsn estudianteBsn = new EstudianteBsn();

    @FXML
    private void initialize() {
        List<Estudiante> estudiantes = estudianteBsn.listarEstudiantes();
        ObservableList<Estudiante> estudiantesObservables = FXCollections.observableList(estudiantes);
        this.cmbEstudiantes.setItems(estudiantesObservables);

        clmDescripcion.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getDescripcion()));
        clmPorcentaje.setCellValueFactory(cellData -> new SimpleDoubleProperty(cellData.getValue().getPorcentaje()).asObject());
        clmNota.setCellValueFactory(cellData -> new SimpleDoubleProperty(cellData.getValue().getValor()).asObject());

    }

    public void cmbEstudiantes_action() {
        Estudiante estudianteSeleccionado = cmbEstudiantes.getValue();
        if (estudianteSeleccionado == null) {
            return;
        }

        List<Nota> notas = this.estudianteBsn.consultarNotas(estudianteSeleccionado.getId());
        ObservableList<Nota> notasObservables = FXCollections.observableList(notas);
        tblNotas.setItems(notasObservables);

        double promedioAcumulado = this.estudianteBsn.obtenerPromedioAcumulado(estudianteSeleccionado.getId());
        lblPromedioAcumulado.setText(String.valueOf(promedioAcumulado));

        double promedioGeneral = this.estudianteBsn.obtenerPromedioGeneral(estudianteSeleccionado.getId());
        lblPromedioGeneral.setText(String.valueOf(promedioGeneral));


    }

    public void btnRegistrarNota_action() {
        Estudiante estudianteSeleccionado = cmbEstudiantes.getValue();
        if (estudianteSeleccionado == null) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Registro de nota");
            alert.setHeaderText("Registro de nota");
            alert.setContentText("Seleccione un estudiante");
            alert.showAndWait();
            return;
        }

        String descripcionIngresada = txtDescripcion.getText().trim();
        String porcentajeIngresado = txtPorcentaje.getText().trim();
        String valorIngresado = txtValor.getText().trim();
        boolean esValido = validarCampos(descripcionIngresada, porcentajeIngresado, valorIngresado);

        if (!esValido) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Registro de nota");
            alert.setHeaderText("Registro de nota");
            alert.setContentText("Diligencie todos los campos");
            alert.showAndWait();
            return;
        }

        double porcentajeDouble = 0.0;
        try {
            porcentajeDouble = Double.parseDouble(porcentajeIngresado);
        } catch (NumberFormatException nfe) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Registro de nota");
            alert.setHeaderText("Porcentaje");
            alert.setContentText("Ingrese un valor numérico");
            txtPorcentaje.requestFocus();
            alert.showAndWait();
            return;
        }

        double valorDouble = 0.0;
        try {
            valorDouble = Double.parseDouble(valorIngresado);
        } catch (NumberFormatException nfe) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Registro de nota");
            alert.setHeaderText("Valor");
            alert.setContentText("Ingrese un valor numérico");
            txtValor.requestFocus();
            alert.showAndWait();
            return;
        }

        Nota nota = new Nota(descripcionIngresada, porcentajeDouble, valorDouble);
        nota.setIdEstudiante(estudianteSeleccionado.getId());
        try {
            this.estudianteBsn.registrarNota(nota);
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Registro de nota");
            alert.setHeaderText("Resultado de la operación");
            alert.setContentText("El registro ha sido exitoso");
            alert.showAndWait();
            limpiarCampos();
        } catch (ObjetoYaExisteException oyee) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Registro de nota");
            alert.setHeaderText("Ha ocurrido un error");
            alert.setContentText(oyee.getMessage());
            alert.showAndWait();
        } catch (NegocioException ne) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Registro de nota");
            alert.setHeaderText("Ha ocurrido un error");
            alert.setContentText(ne.getMessage());
            alert.showAndWait();
        }
    }

    private boolean validarCampos(String... campos) {
        for (int i = 0; i < campos.length; i++) {
            if (campos[i] == null || "".equals(campos[i])) {
                return false;
            }
        }
        return true;
    }

    public void limpiarCampos() {
        cmbEstudiantes.setValue(null);
        txtValor.clear();
        txtDescripcion.clear();
        txtPorcentaje.clear();
    }

}
