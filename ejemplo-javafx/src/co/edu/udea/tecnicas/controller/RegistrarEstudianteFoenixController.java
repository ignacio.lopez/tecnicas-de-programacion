package co.edu.udea.tecnicas.controller;

import co.edu.udea.tecnicas.bsn.EstudianteBsn;
import co.edu.udea.tecnicas.bsn.exception.ObjetoYaExisteException;
import co.edu.udea.tecnicas.model.Estudiante;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.validation.RequiredFieldValidator;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextFormatter;
import org.kordamp.ikonli.fontawesome5.FontAwesomeSolid;
import org.kordamp.ikonli.javafx.FontIcon;

public class RegistrarEstudianteFoenixController {

    @FXML
    private JFXTextField txtIdentificacion;
    @FXML
    private JFXTextField txtNombres;
    @FXML
    private JFXTextField txtApellidos;
    @FXML
    private JFXTextField txtProgramaAcademico;

    private EstudianteBsn estudianteBsn = new EstudianteBsn();

    @FXML
    public void initialize() {
        txtIdentificacion.setTextFormatter(new TextFormatter<>(change -> {
            if (change.getControlNewText().matches("([1-9][0-9]*)?") && change.getControlNewText().length() <= 12) {
                return change;
            }
            return null;
        }));
        RequiredFieldValidator validator = new RequiredFieldValidator();
        validator.setMessage("Campo requerido");
        FontIcon warnIcon = new FontIcon(FontAwesomeSolid.ARROW_UP.getDescription());
        validator.setIcon(warnIcon);

        txtIdentificacion.getValidators().add(validator);
        txtIdentificacion.focusedProperty().addListener((o, oldVal, newVal) -> {
            if (!newVal) txtIdentificacion.validate();
        });

        txtNombres.getValidators().add(validator);
        txtNombres.focusedProperty().addListener((o, oldVal, newVal) -> {
            if (!newVal) txtNombres.validate();
        });

        txtApellidos.getValidators().add(validator);
        txtApellidos.focusedProperty().addListener((o, oldVal, newVal) -> {
            if (!newVal) txtApellidos.validate();
        });

        txtProgramaAcademico.getValidators().add(validator);
        txtProgramaAcademico.focusedProperty().addListener((o, oldVal, newVal) -> {
            if (!newVal) txtProgramaAcademico.validate();
        });


    }


    public void btnGuardar_action() {
        if (validarCampos()) {

            String idIngresado = txtIdentificacion.getText().trim();
            String nombresIngresados = txtNombres.getText().trim();
            String apellidosIngresados = txtApellidos.getText().trim();
            String programaAcademicoIngresado = txtProgramaAcademico.getText().trim();

            Estudiante estudiante = new Estudiante(idIngresado, nombresIngresados, apellidosIngresados, programaAcademicoIngresado);

            try {
                estudianteBsn.registrarEstudiante(estudiante);
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Registro de estudiante");
                alert.setHeaderText("Resultado de la operación");
                alert.setContentText("El registro ha sido exitoso");
                alert.showAndWait();
                limpiarCampos();
            } catch (ObjetoYaExisteException oyee) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Registro de estudiante");
                alert.setHeaderText("Ha ocurrido un error");
                alert.setContentText(oyee.getMessage());
                alert.showAndWait();
            }
        }


    }

    private boolean validarCampos() {
        if (!txtIdentificacion.validate()) {
            mostrarMensaje("Identificación");
            txtIdentificacion.requestFocus();
            return false;
        }
        if (!txtNombres.validate()) {
            mostrarMensaje("Nombres");
            txtNombres.requestFocus();
            return false;
        }
        if (!txtApellidos.validate()) {
            mostrarMensaje("Apellidos");
            txtApellidos.requestFocus();
            return false;
        }
        if (!txtProgramaAcademico.validate()) {
            mostrarMensaje("Programa académico");
            txtProgramaAcademico.requestFocus();
            return false;
        }
        return true;
    }

    private void mostrarMensaje(String campo) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Registro de estudiante");
        alert.setHeaderText(campo);
        alert.setContentText("Revise la información");
        alert.showAndWait();
        ;
    }

    private void limpiarCampos() {
        txtIdentificacion.clear();
        txtNombres.clear();
        txtApellidos.clear();
        txtProgramaAcademico.clear();
    }

}
