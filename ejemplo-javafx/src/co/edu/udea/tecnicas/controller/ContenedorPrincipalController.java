package co.edu.udea.tecnicas.controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.IOException;

public class ContenedorPrincipalController {

    @FXML
    private BorderPane contenedorPrincipal;
    private Stage primaryStage;


    @FXML
    public void initialize() {
        /*try {
            AnchorPane inicio = FXMLLoader
                    .load(getClass().getResource("../view/inicio.fxml"));
            this.contenedorPrincipal.setCenter(inicio);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }*/
    }

    public void mnuSalir_action() {
        System.exit(0);
    }

    public void mnuRegistrarEstudiante_action() {
        try {
            AnchorPane registrarEstudiante = FXMLLoader
                    .load(getClass().getResource("../view/registrar-estudiante.fxml"));
            this.contenedorPrincipal.setCenter(registrarEstudiante);
            this.primaryStage.setWidth(700);
            this.primaryStage.setHeight(700);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

    }

    public void mnuRegistraEstudianteFoenix_action() {
        try {
            AnchorPane registrarEstudiante = FXMLLoader
                    .load(getClass().getResource("../view/registrar-estudiante-foenix.fxml"));
            Scene scene = new Scene(registrarEstudiante);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.show();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public void mnuRegistrarNota_action() {
        try {
            AnchorPane registrarNota = FXMLLoader
                    .load(getClass().getResource("../view/registrar-nota.fxml"));
            this.contenedorPrincipal.setCenter(registrarNota);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public void mnuAcercaDe_action() {

    }

    public void setStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }
}
