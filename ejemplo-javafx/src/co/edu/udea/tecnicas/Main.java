package co.edu.udea.tecnicas;

import co.edu.udea.tecnicas.controller.ContenedorPrincipalController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{

        FXMLLoader loader = new FXMLLoader(getClass().getResource("view/contenedor-principal.fxml"));
        Parent root = loader.load();

        ContenedorPrincipalController cpc = loader.getController();
        cpc.setStage(primaryStage);

        primaryStage.setTitle("Universidad");
        primaryStage.setScene(new Scene(root, 600, 400));
        primaryStage.show();

    }


    public static void main(String[] args) {
         launch(args);
    }
}
