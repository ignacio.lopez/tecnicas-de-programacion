package co.edu.udea.tecnicas.model;

import java.util.List;

public class Estudiante {

    // atributos
    private String id;
    private String nombres;
    private String apellidos;
    private String programaAcademico;

    // relaciones
    private List<Nota> notas;

    public Estudiante(String id, String nombres, String apellidos, String programaAcademico) {
        this.id = id;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.programaAcademico = programaAcademico;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getProgramaAcademico() {
        return programaAcademico;
    }

    public void setProgramaAcademico(String programaAcademico) {
        this.programaAcademico = programaAcademico;
    }

    public List<Nota> getNotas() {
        return notas;
    }

    public void setNotas(List<Nota> notas) {
        this.notas = notas;
    }

    public String toString(){
        return String.format("%s - %s", this.id, this.nombres);
    }

}
