package co.edu.udea.tecnicas.model;

public class Nota {

    // atributos
    private String descripcion;
    private double porcentaje;
    private double valor;

    // relaciones
    private Estudiante estudiante; // objeto
    private String idEstudiante; // clave (opcional)


    public Nota(String descripcion, double porcentaje, double valor) {
        this.descripcion = descripcion;
        this.porcentaje = porcentaje;
        this.valor = valor;
    }

    public double getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(double porcentaje) {
        this.porcentaje = porcentaje;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Estudiante getEstudiante() {
        return estudiante;
    }

    public void setEstudiante(Estudiante estudiante) {
        this.estudiante = estudiante;
    }

    public String getIdEstudiante() {
        return idEstudiante;
    }

    public void setIdEstudiante(String idEstudiante) {
        this.idEstudiante = idEstudiante;
    }
}
