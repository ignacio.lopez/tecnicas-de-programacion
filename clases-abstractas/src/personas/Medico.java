package personas;

public class Medico extends Empleado {

    private String especializacion;

    public Medico(String identificacion, String nombres, String apellidos, int rangoSalarial, String especializacion) {
        super(identificacion, nombres, apellidos, rangoSalarial);
        this.especializacion = especializacion;
    }


    @Override
    public String decirProfesion() {
        return "Soy médico";
    }
}
