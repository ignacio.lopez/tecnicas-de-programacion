package personas;

public abstract class Persona {
    protected String identificacion;
    protected String nombres;
    protected String apellidos;

    public Persona(String identificacion, String nombres, String apellidos) {
        this.identificacion = identificacion;
        this.nombres = nombres;
        this.apellidos = apellidos;
    }


    // método concreto (método con código)
    public String presentarse(){
        return String.format("Hola, soy %s %s", nombres, apellidos);
    }

    // método abstracto (sin código)
    public  String decirProfesion(){
        return "Soy persona";
    }


    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }
}
