package personas;

public class Estudiante extends Persona {
    private double promedio;
    private String programaAcademico;

    public Estudiante(String identificacion, String nombres, String apellidos, double promedio, String programaAcademico) {
        super(identificacion, nombres, apellidos);
        this.promedio = promedio;
        this.programaAcademico = programaAcademico;
    }


    @Override
    public String decirProfesion() {
        return "Soy estudiante";
    }

    public void cambiarNombres(){

        this.nombres = "otra cosa";
    }

}
