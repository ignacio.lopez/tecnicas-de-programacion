package personas;

public abstract class Empleado extends Persona {

    private int rangoSalarial;

    public Empleado(String identificacion, String nombres, String apellidos, int rangoSalarial) {
        super(identificacion, nombres, apellidos);
        this.rangoSalarial = rangoSalarial;
    }
}
