package model.builder;

import model.Curso;

public final class CursoBuilder {

    private int codigo;
    private String nombre;
    private int numeroCreditos;

    public CursoBuilder() {
    }

    public CursoBuilder withCodigo(int codigo) {
        this.codigo = codigo;
        return this;
    }

    public CursoBuilder withNombre(String nombre) {
        this.nombre = nombre;
        return this;
    }

    public CursoBuilder withNumeroCreditos(int numeroCreditos) {
        this.numeroCreditos = numeroCreditos;
        return this;
    }

    public Curso build() {
        Curso curso = new Curso();
        curso.setCodigo(this.codigo);
        curso.setNombre(this.nombre);
        curso.setNumeroCreditos(this.numeroCreditos);
        return curso;
    }
}
