package model.builder;

import model.Estudiante;

public class EstudianteBuilder {

    //atributos
    private String id;
    private String nombres;
    private char genero;
    private int programaAcademico;


    public EstudianteBuilder(){

    }

    public EstudianteBuilder withId(String id){
        this.id=id;
        return this;
    }

    public EstudianteBuilder withNombres(String nombres){
        this.nombres=nombres;
        return this;
    }

    public EstudianteBuilder withGenero(char genero){
        this.genero=genero;
        return this;
    }

    public EstudianteBuilder withProgramaAcademico(int programaAcademico){
        this.programaAcademico=programaAcademico;
        return this;
    }

    public Estudiante build(){
        Estudiante e = new Estudiante();
        e.setId(this.id);
        e.setNombres(this.nombres);
        e.setGenero(this.genero);
        e.setProgramaAcademico(this.programaAcademico);
        return e;
    }
}
