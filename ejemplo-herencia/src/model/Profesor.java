package model;

import java.time.LocalDate;

public class Profesor extends Persona{

    private String profesion;
    private double salario;

    public static int contadorProfesoresEstatica=0;

    public int getContadorProfesoresNoEstatica() {
        return contadorProfesoresNoEstatica;
    }

    private int contadorProfesoresNoEstatica=0;

    public Profesor(int id, String nombres, String apellidos, LocalDate fechaNacimiento,
                    String profesion, double salario) {
        this.contadorProfesoresNoEstatica++;
        contadorProfesoresEstatica++;
        this.id=id;
        this.nombres=nombres;
        this.apellidos=apellidos;
        this.fechaNacimiento=fechaNacimiento;
        this.profesion = profesion;
        this.salario = salario;
    }

    public String getProfesion() {
        return profesion;
    }

    public void setProfesion(String profesion) {
        this.profesion = profesion;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }
}
