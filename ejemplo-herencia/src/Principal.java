import model.Alumno;
import model.Persona;
import model.Profesor;

import java.time.LocalDate;

public class Principal {

    public int variablePropia =0;

    public static void main(String[] args) {

        Principal principal = new Principal();
        principal.variablePropia++;

        // ESTO DA ERROR
        // this.variablePropia++;
        System.out.println(Profesor.contadorProfesoresEstatica);
        Persona persona = new Persona();


        Alumno alumno = new Alumno("Sistemas", 2.8);


        Profesor profesor = new Profesor(123, "a", "b",
                LocalDate.of(1980, 01, 01), "Ingeniero de sistemas", 200_000);

        System.out.println(profesor.getContadorProfesoresNoEstatica());
        System.out.println(Profesor.contadorProfesoresEstatica);

        Profesor profesor2 = new Profesor(123, "a", "b",
                LocalDate.of(1980, 01, 01), "Ingeniero de sistemas", 200_000);

        System.out.println(profesor2.getContadorProfesoresNoEstatica());
        System.out.println(Profesor.contadorProfesoresEstatica);

    }
}
