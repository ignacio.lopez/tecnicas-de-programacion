public class Ave implements Volador{
    private String especie;
    private String color;



    public String getEspecie() {
        return especie;
    }

    public void setEspecie(String especie) {
        this.especie = especie;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String volar() {
        return "Vuelo como ave";
    }
}




