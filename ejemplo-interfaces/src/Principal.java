public class Principal {

    public static void main(String[] args) {
        // Persona p = new Persona();
        Ave a = new Ave();
        System.out.println(a.volar());


        Persona empleado = new Empleado();
        // empleado.calcularSalario();


        // Volador v =  new Volador();

        Volador v = new Estudiante();
        // v.matricularCurso();

        System.out.println(v.volar());

        //top-down casting
        Estudiante e = (Estudiante) v;
        System.out.println(e.decirProfesion());

        v = new Ave();
        System.out.println(v.volar());

    }
}
