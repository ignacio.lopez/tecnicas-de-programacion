package sobrecarga;

public class Sobrecarga {

    public static int sumar(int numero1, int numero2){
        return numero1+numero2;
    }

    public static int sumar(int[] numeros){
        int suma =0;
        for(int i=0; i<numeros.length; i++){
            suma+=numeros[i];
        }
        return suma;
    }


    public static double sumar(double numero1, double numero2){
        return numero1+numero2;
    }


}
