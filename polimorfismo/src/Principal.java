import liskov.Animal;
import liskov.Gato;
import liskov.Perro;

public class Principal {

    public static void main(String[] args) {
      /*  System.out.println(Sobrecarga.sumar(1,2));
        System.out.println(Sobrecarga.sumar(new int[]{1,2,3,4,5,6}));
        System.out.println(Sobrecarga.sumar(1.5,  2.8));

        Madre madre = new Madre();
        System.out.println(madre.saludar());

        Hija hija = new Hija();
        System.out.println(hija.saludar());

        Animal animal1 = new Perro();
        System.out.println(animal1.hacerRuido());
        Animal animal2 = new Gato();
        System.out.println(animal2.hacerRuido());

        Animal[] arregloAnimales = {new Perro(), new Gato(), new Gato(), new Perro()};

        Object object = new Perro();


        Empleado empleado = new Empleado("Empleado 1", 25000);
        System.out.println(empleado.calcularSalario(20));

        Empleado empleado2 = new Empleado("Empleado 2", 20000);
        System.out.println(empleado2.calcularSalario(10));


        */

        Animal a = new Animal();
        Animal b = new Perro();
        Gato g = new Gato();

        System.out.println(a.hacerRuido());
        System.out.println(b.hacerRuido());
        // System.out.println(b.pasear());

        Perro p = (Perro) b; // top-down casting
        System.out.println(p.pasear());

        Object o = a; // bottom-up casting
        // System.out.println(o.hacerRuido());

        Animal q = g; // bottom-up casting
        // System.out.println(q.araniarMuebles());

    }
}
